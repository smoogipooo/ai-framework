﻿using System.Collections.Generic;
using AIFramework.Helpers;

namespace AIFramework.Actions
{
    public class ActionData
    {
        private Point2 position;
        private Dictionary<string, Comparable> extraData;

        public ActionData(Point2 agentPosition, Dictionary<string, Comparable> data = null)
        {
            position = agentPosition;
            extraData = data;
        }

        public float CompareTo(ActionData other)
        {
            float publicResult = position.DistanceTo(other.position);

            if (extraData != null)
            {
                foreach (KeyValuePair<string, Comparable> data in extraData)
                {
                    Comparable otherData;
                    if (other.extraData.TryGetValue(data.Key, out otherData))
                        publicResult += data.Value.CompareTo(otherData);
                }
            }

            return publicResult;
        }
    }

    public class Action
    {
        public string Type;
        private ActionData data;
        public float Evaluation = float.MinValue;

        public Action(string type, ActionData data)
        {
            Type = type;
            this.data = data;
        }

        public float CompareTo(Action other)
        {
            return Evaluation.DistanceTo(other.Evaluation) + data.CompareTo(other.data);
        }

    }
}