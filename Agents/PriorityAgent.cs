﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using AIFramework.Actions;
using AIFramework.Helpers;
using AIFramework.Rulesets;
using Action = AIFramework.Actions.Action;

namespace AIFramework.Agents
{
    public enum AgentState
    {
        Initializing,
        Running,
        Stopped
    }

    public class PriorityAgent
    {
        private Ruleset ruleset;
        private Random random = new Random();
        private List<KeyValuePair<Action, float>> previousActions = new List<KeyValuePair<Action, float>>();

        public Point2 Position;
        public AgentState State = AgentState.Initializing;

        public virtual void Initialize(Ruleset r)
        {
            ruleset = r;
        }

        public virtual void StartAgent()
        {
            State = AgentState.Running;
        }

        public virtual void StopAgent()
        {
            State = AgentState.Stopped;
        }

        public virtual void Update()
        {
            if (State != AgentState.Running)
                return;

            if (previousActions.Count == 0)
            {
                performRandomAction();
                return;
            }

            int count = ruleset.Actions.Length;

            Action nextAction = null;
            float nextEvaluation = float.MinValue;
            float closestComparison = float.MinValue;
            float closestEvaluation = float.MinValue;
            Action closestAction = null;

            Dictionary<string, Comparable> currentComparables = ruleset.GetComparables();
            for (int i = 0; i < count; i++)
            {
                string newType = ruleset.Actions[i];
                Action newAction = new Action(newType, new ActionData(Position, currentComparables));

                //Find the most relevant of the previously performed actions
                //and compare the new suggested action to it
                foreach (KeyValuePair<Action, float> kvp in previousActions)
                {
                    float comparison = newAction.CompareTo(kvp.Key);
                    if (comparison >= closestComparison)
                    {
                        closestComparison = comparison;
                        closestEvaluation = ruleset.Evaluate(this, kvp.Key);
                        closestAction = kvp.Key;
                    }
                }

                float eval = ruleset.Evaluate(this, newAction);

                //Accept evaluation only if previous action is deemed unreasonable
                //i.e. ruleset tells agent to evolve or previous action is not applicable
                if (eval > closestEvaluation && eval > nextEvaluation)
                {
                    nextEvaluation = eval;
                    nextAction = newAction;
                }
                else if (closestEvaluation > nextEvaluation)
                {
                    nextAction = closestAction;
                    nextEvaluation = closestEvaluation;
                }
            }

            //If agent has calculated an evaluation but doesn't like it
            //it will perform a random action to build up its priority queue
            if (nextEvaluation < 0)
                performRandomAction();
            else
                performAction(nextAction, nextEvaluation);
        }

        private void performRandomAction()
        {
            Action randomAction = new Action(ruleset.Actions[random.Next(0, ruleset.Actions.Length)], new ActionData(Position, ruleset.GetComparables()));
            performAction(randomAction, ruleset.Evaluate(this, randomAction));
        }

        private void performAction(Action action, float evaluation)
        {
            action.Evaluation = evaluation;
            ruleset.Perform(this, action);
            for (int i = 0; i < previousActions.Count; i++)
            {
                previousActions[i] = new KeyValuePair<Action, float>(previousActions[i].Key, previousActions[i].Value / 2);
                if (previousActions[i].Value < 0.1)
                    previousActions.RemoveAt(i);
            }
            previousActions.Add(new KeyValuePair<Action, float>(action, evaluation));
        }
    }
}
