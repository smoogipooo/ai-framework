﻿using System;

namespace AIFramework.Helpers
{
    public static class DistanceHelpers
    {
        public static float DistanceTo(this float value1, float value2)
        {
            return (float)Math.Sqrt(Math.Pow(value1 - value2, 2));
        }

        public static float DistanceTo(this byte value1, byte value2)
        {
            return (float)Math.Sqrt(Math.Pow(value1 - value2, 2));
        }
    }

    public abstract class Comparable
    {
        public abstract float CompareTo(Comparable other);
    }

    public class ComparablePoint : Comparable
    {
        public Point2 Position;

        public override float CompareTo(Comparable other)
        {
            ComparablePoint otherComparable = other as ComparablePoint;

            return otherComparable == null ? float.MinValue : Position.DistanceTo(otherComparable.Position);
        }

        public ComparablePoint(Point2 position)
        {
            Position = position;
        }
    }
}
