﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIImageReisizing.Helpers
{
    public class ConcurrentList<T> : IEnumerable<T>
    {
        private List<T> list = new List<T>();
        private object listLock = new object();

        public ConcurrentList()
        { }

        public ConcurrentList(int capacity)
        {
            list.Capacity = capacity;
        }

        public ConcurrentList(IEnumerable<T> collection)
        {
            list.AddRange(collection);
        }

        public void Add(T value)
        {
            lock (listLock)
                list.Add(value);
        }

        public void Insert(int index, T value)
        {
            lock (listLock)
                list.Insert(index, value);
        }

        public void Clear()
        {
            lock (listLock)
                list.Clear();
        }

        public bool Remove(T value)
        {
            lock (listLock)
                return list.Remove(value);
        }

        public void RemoveAt(int index)
        {
            lock (listLock)
                list.RemoveAt(index);
        }

        public T Find(Predicate<T> match)
        {
            lock (listLock)
                return list.Find(match);
        }

        public int Capacity
        {
            get
            {
                lock (listLock)
                    return list.Capacity;
            }
            set
            {
                lock (listLock)
                    list.Capacity = value;
            }
        }

        public int Count 
        { 
            get 
            {
                lock (listLock)
                    return list.Count; 
            } 
        }

        public T this[int i]
        {
            get 
            {
                lock (listLock)
                {
                    if (i >= list.Count && list.Count > 0)
                        return list[list.Count - 1];
                    if (i < list.Count && list.Count > 0)
                        return list[i];
                    return default(T);
                }
            }
            set
            {
                lock (listLock)
                    list[i] = value;
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            lock (listLock)
                return list.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            lock (listLock)
                return GetEnumerator();
        }
    }
}
