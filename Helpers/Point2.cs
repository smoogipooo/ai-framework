﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AIFramework.Helpers
{
    public class Point2
    {
        public float X = 0;
        public float Y = 0;

        public static Point2 One = new Point2(0);
        public static Point2 Zero = new Point2(1);

        public Point2()
        { }
        
        public Point2(float xy)
        {
            X = Y = xy;
        }

        public Point2(float x, float y)
        {
            X = x;
            Y = y;
        }

        public Point2(Point2 other)
        {
            X = other.X;
            Y = other.Y;
        }

        public void Normalize()
        {
            float length = (float)Math.Sqrt(Math.Pow(X, 2) + Math.Pow(Y, 2));
            if (length == 0)
                return;
            X = X / length;
            Y = Y / length;
        }

        public static Point2 Normalize(Point2 point)
        {
            float length = (float)Math.Sqrt(Math.Pow(point.X, 2) + Math.Pow(point.Y, 2));
            if (length == 0)
                return Point2.Zero;
            point.X = point.X / length;
            point.Y = point.Y / length;
            return point;
        }

        public float DistanceTo(Point2 other)
        {
            return (float)Math.Sqrt(DistanceToSquared(other));
        }

        public float DistanceToSquared(Point2 other)
        {
            return (float)(Math.Pow(other.X - X, 2) + Math.Pow(other.Y - Y, 2));
        }

        #region Operators

        public static Point2 operator+(Point2 pt1, Point2 pt2)
        {
            return new Point2(pt1.X + pt2.X, pt1.Y + pt2.Y);
        }

        public static Point2 operator-(Point2 pt1, Point2 pt2)
        {
            return new Point2(pt1.X - pt2.X, pt1.Y - pt2.Y);
        }

        public static Point2 operator*(Point2 pt1, float val)
        {
            return new Point2(pt1.X * val, pt1.Y * val);
        }

        public static Point2 operator/(Point2 pt1, float val)
        {
            return new Point2(pt1.X / val, pt1.Y / val);
        }

        public static bool operator==(Point2 pt1, Point2 pt2)
        {
            return pt1.X == pt2.X && pt1.Y == pt2.Y;
        }

        public static bool operator!=(Point2 pt1, Point2 pt2)
        {
            return !(pt1 == pt2);
        }

        public override bool Equals(object obj)
        {
            Point2 other = obj as Point2;
            if (other == null)
                return false;
            return X == other.X && Y == other.Y;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + X.GetHashCode();
                hash = hash * 23 + Y.GetHashCode();
                return hash;
            }
        }

        #endregion
    }
}
