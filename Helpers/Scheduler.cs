﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AIFramework.Helpers
{
    public static class Scheduler
    {
        /// <summary>
        /// Schedules one work item to run (a)synchronously.
        /// </summary>
        public static void Add(Task action, bool awaitCompletion = false)
        {
            ManualResetEvent workItem = new ManualResetEvent(false);
            Action workAction = delegate
            {
                try
                {
                    if (!action.IsCanceled)
                        action.RunSynchronously();
                }
                finally
                {
                    workItem.Set();
                }
            };
            ThreadPool.QueueUserWorkItem(delegate { workAction(); });
            if (awaitCompletion)
                workItem.WaitOne();
        }

        /// <summary>
        /// Schedules one work item to run (a)synchronously.
        /// </summary>
        public static void Add(Action action, bool awaitCompletion = false)
        {
            Add(new Task(action), awaitCompletion);
        }

        /// <summary>
        /// Schedules multiple actions to run (a)synchronously.
        /// </summary>
        public static void Add(List<Action> actions, bool awaitCompletion = false)
        {
            ManualResetEvent workItem = new ManualResetEvent(false);
            int workCount = actions.Count;

            for (int i = 0; i < actions.Count(); i++)
            {
                int localI = i;
                Action workAction = delegate 
                { 
                    try
                    {
                        actions[localI]();
                    }
                    finally
                    {
                        if (Interlocked.Decrement(ref workCount) == 0)
                            workItem.Set();
                    }
                };
                ThreadPool.QueueUserWorkItem(delegate { workAction(); });
            }

            if (awaitCompletion)
                workItem.WaitOne();
        }
    }
}
