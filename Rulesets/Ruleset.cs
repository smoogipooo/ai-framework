﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AIFramework.Actions;
using AIFramework.Agents;
using AIFramework.Helpers;
using Action = AIFramework.Actions.Action;

namespace AIFramework.Rulesets
{
    public abstract class Ruleset
    {
        internal string[] Actions;
        public PriorityAgent CurrentAgent { get; private set; }

        public Ruleset()
        {
            CurrentAgent = new PriorityAgent();
        }

        public virtual void Initialize(string[] rulesetActions)
        {
            Actions = rulesetActions;
            CurrentAgent.Initialize(this);
        }

        public virtual void Update()
        {
            CurrentAgent.Update();
        }

        public abstract Dictionary<string, Comparable> GetComparables();

        /// <summary>
        /// Evaluates the action in context to the rest of the ruleset.
        /// </summary>
        /// <param name="action">The action to evaluate.</param>
        /// <returns>The expected evaluation of the action.</returns>
        public abstract float Evaluate(PriorityAgent agent, Action action);

        /// <summary>
        /// Performs an action.
        /// </summary>
        /// <param name="action">The action to perform.</param>
        public abstract void Perform(PriorityAgent agent, Action action);

        public virtual void Begin()
        {
            CurrentAgent.StartAgent();
        }
    }
}
